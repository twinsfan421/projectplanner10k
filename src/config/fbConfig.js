import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'

var firebaseConfig = {
    apiKey: "AIzaSyArs4UFmgdtCi01q3AHz3r8gvGDHuBT5pc",
    authDomain: "project-planner-10k.firebaseapp.com",
    databaseURL: "https://project-planner-10k.firebaseio.com",
    projectId: "project-planner-10k",
    storageBucket: "",
    messagingSenderId: "686566070605",
    appId: "1:686566070605:web:e5215dbf36e0d2f9"
};

firebase.initializeApp(firebaseConfig);
firebase.firestore()

export default firebase;