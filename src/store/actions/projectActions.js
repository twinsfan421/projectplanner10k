export const createProject = (project) => {
    return (dispatch, getState, { getFirebase, getFirestore}) => {
        const firestore = getFirestore();
        const profile = getState().firebase.profile;
        const authorId = getState().firebase.auth.uid;
        firestore.collection('projects').add({
            ...project, 
            authorFirstName: profile.firstName,
            authorLastName: profile.lastName,
            authorId: authorId,
            createdAt: new Date()
        }).then(() => {
            dispatch({type: 'CREATE_PROJECT', project});
        }).catch((err) => {
            dispatch({ type: 'CREATE_PROJECT_ERROR', err})
        })
    }
};

export const projectEdit = (project) => {
    return (dispatch, getState, { getFirebase, getFirestore}) => {
        const firestore = getFirestore();
        firestore.collection('projects').doc(project.id).update({ 
            title: project.title,
            content: project.content,
            updatedAt: new Date()
        }).then(() => {
            dispatch({type: 'PROJECT_EDIT', project});
        }).catch((err) => {
            dispatch({ type: 'PROJECT_EDIT_ERROR', err})
        })
    }
};

export const projectDelete = (projectId) => {
    return (dispatch, getState, { getFirebase, getFirestore}) => {
        const firestore = getFirestore();
        firestore.collection('projects').doc(projectId).delete().then(() => {
            dispatch({type: 'PROJECT_DELETE'});
        }).catch((err) => {
            dispatch({ type: 'PROJECT_DELETE_ERROR', err})
        })
    }
};