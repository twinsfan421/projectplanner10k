const initSate = {
    projects: [
        {id: '1', title: 'why minnesota', content: 'its the best'},
        {id: '2', title: 'why not minnesota', content: 'no mountains'},
        {id: '3', title: 'go vikes', content: 'skol'}
    ]
}

const projectReducer = (state = initSate, action) => {
    switch (action.type) {
        case 'CREATE_PROJECT':
            console.log('created project', action.project)
            return state;
        case 'CREATE_PROJECT_ERROR':
            console.log('create project error', action.err)
            return state;
        case 'PROJECT_EDIT':
            console.log('project edit', action.project)
            return state;
        case 'PROJECT_EDIT_ERROR':
            console.log('project edit error', action.err)
            return state;
        case 'PROJECT_DELETE':
            console.log('project deleted')
            return state;
        case 'PROJECT_DELETE_ERROR':
            console.log('project delete error', action.err)
            return state;
        default:
            return state;
    }
}

export default projectReducer