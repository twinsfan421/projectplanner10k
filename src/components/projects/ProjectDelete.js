import React, { Component } from 'react'
import { connect } from 'react-redux'
import { projectDelete } from '../../store/actions/projectActions'
import { Redirect } from 'react-router-dom'
import { firestoreConnect } from 'react-redux-firebase'
import { compose } from 'redux'
import { Link } from "react-router-dom"

class ProjectDelete extends Component {

    deleteSubmit = (e) => {
        this.props.projectDelete(this.props.id)
        this.props.history.push('/');
    }
    render() {
        const { auth, project, id } = this.props;
        if (!auth.uid) return <Redirect to='/signin' />
        if (project) {
            if (project.authorId !== auth.uid) return <Redirect to='/' />
            return (
                <div className="container section project-details">
                    <h3>Are you sure you want to delete this Project?</h3>
                    <div className="card z-depth-0">
                        <div className="card-content">
                            <span className="card-title">{ project.title }</span>
                            <p>{ project.content }</p>
                        </div>
                        <div className="card-action grey lighten-4 grey-text">
                            <button onClick={this.deleteSubmit} className="btn-large blue lighten-2 z-depth-0">Delete Project</button>
                        </div>
                    </div>
                    <Link to={'/project/' + id} >
                        <button className="btn orange">Go Back</button>
                    </Link>
                </div>
            )
        } else {
            return (
                <div className="container center">
                    <p>Loading Project Delete...</p>
                </div>
            )
        }
    }
}

const mapStateToProps = (state, ownProps) => {
    const id = ownProps.match.params.id;
    const projects = state.firestore.data.projects;
    const project = projects ? projects[id] : null
    return {
        auth: state.firebase.auth,
        project: project,
        id: id
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        projectDelete: (project) => dispatch(projectDelete(project))
    }
}

export default compose(
    connect(mapStateToProps, mapDispatchToProps),
    firestoreConnect([
        { collection: 'projects' }
    ])
)(ProjectDelete);