import React from 'react';
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'
import { compose } from 'redux'
import { Redirect } from 'react-router-dom'
import moment from 'moment'
import { Link } from 'react-router-dom'

const ProjectDetails = (props) => {
    const { project, auth, id } = props;
    if (!auth.uid) return <Redirect to='/signin' />

    if (project) {
        return (
            <div className="container section project-details">
                <div className="card z-depth-0">
                    <div className="card-content">
                        <span className="card-title">{ project.title }</span>
                        <p>{ project.content }</p>
                    </div>
                    <div className="card-action grey lighten-4 grey-text">
                        <div>Posteby by {project.authorFirstName} {project.authorLastName}</div>
                        <div>Created at:{moment(project.createdAt.toDate()).calendar() }</div>
                        {project.updatedAt ? <div className="red-text">
                            Updated at:{moment(project.updatedAt.toDate()).calendar() }</div> : null }
                    </div>
                </div>
                <Link to={'/'} >
                    <button className="btn orange">Go Back</button>
                </Link>
                {project.authorId === auth.uid ?
                    <Link to={'/projectedit/' + id} >
                      <button className="btn orange">Edit Project</button>
                    </Link> : null }
                {project.authorId === auth.uid ?
                    <Link to={'/projectdelete/' + id} >
                      <button className="btn orange">Delete Project</button>
                    </Link> : null }
            </div>
        )
    } else {
        return (
            <div className="container center">
                <p>Loading project details...</p>
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    const id = ownProps.match.params.id;
    const projects = state.firestore.data.projects;
    const project = projects ? projects[id] : null
    return {
        project: project,
        auth: state.firebase.auth,
        id: id
    }
}

export default compose(
    connect(mapStateToProps),
    firestoreConnect([
        { collection: 'projects' }
    ])
)(ProjectDetails);