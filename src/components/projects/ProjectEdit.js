import React, { Component } from 'react'
import { connect } from 'react-redux'
import { projectEdit } from '../../store/actions/projectActions'
import { Redirect } from 'react-router-dom'
import { firestoreConnect } from 'react-redux-firebase'
import { compose } from 'redux'
import { Link } from "react-router-dom"

class ProjectEdit extends Component {

    handleChange = (e) => {
      this.props.project[e.target.id] = e.target.value
    }

    handleSubmit = (e) => {
        e.preventDefault()
        const project = {
            id: this.props.id,
            title: this.props.project.title,
            content: this.props.project.content
        }
        this.props.projectEdit(project)
        this.props.history.push('/')
    }

    render() {
        const { auth, project, id } = this.props;
        if (!auth.uid) return <Redirect to='/signin' />
        if (project) {
            if (project.authorId !== auth.uid) return <Redirect to='/' />
            return (
                <div className="container">
                    <form onSubmit={this.handleSubmit} className="white">
                        <h5 className="grey-text text-darken-3">Edit Project</h5>
                        <label htmlFor="title">Title</label>
                        <div className="input-field">
                            <input type="text" id="title" defaultValue={project.title} onChange={this.handleChange}/>
                        </div>
                        <label htmlFor="content">Project Content</label>
                        <div className="input-field">
                            <textarea className="materialize-textarea" defaultValue={project.content} id="content"  onChange={this.handleChange}/>
                        </div>
                        <div className="input-field">
                           <button className="btn-large blue lighten-2 z-depth-0">Submit Edit</button>
                        </div>
                    </form>
                    <Link to={'/project/' + id} >
                        <button className="btn orange">Go Back</button>
                    </Link>
                </div>
            )
        } else {
            return (
                <div className="container center">
                    <p>Loading Project Edit...</p>
                </div>
            )
        }
    }
}

const mapStateToProps = (state, ownProps) => {
    const id = ownProps.match.params.id;
    const projects = state.firestore.data.projects;
    const project = projects ? projects[id] : null
    return {
        auth: state.firebase.auth,
        project: project,
        id: id
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        projectEdit: (project) => dispatch(projectEdit(project))
    }
}

export default compose(
    connect(mapStateToProps, mapDispatchToProps),
    firestoreConnect([
        { collection: 'projects' }
    ])
)(ProjectEdit);